<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Status;

class UserController extends Controller
{
    public function Submit(Request $request){
        $arr = array();
        $input = $request->all();
        $result = User::create($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
}
